package com.khai.demo.mutiny

import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.Uni
import io.smallrye.mutiny.tuples.Tuple2
import java.util.concurrent.CompletableFuture

class Service {

    fun getVeges(): Uni<List<String>> = Uni.createFrom().completionStage(
        CompletableFuture.supplyAsync {
            println("Veges > Start")
            Thread.sleep(100)
            println("Veges > Done")
            listOf("Broccoli", "Spinach")
        }
    )
    fun getVegesBlock(): List<String> = getVeges().await().indefinitely()

    fun getMeats(): Uni<List<String>> = Uni.createFrom().completionStage(
        CompletableFuture.supplyAsync {
            println("Meats > Start")
            Thread.sleep(100)
            println("Meats > Done")
            listOf("Chicken", "Beef")
        }
    )

    fun cookFoods(foods: List<String>): Uni<List<String>> = Uni.createFrom().completionStage(
        CompletableFuture.supplyAsync {
            println("Cook  > Start: $foods")
            Thread.sleep(100)
            println("Cook  > Done: $foods")
            foods.map { "Cooked_$it" }
        }
    )

    fun eatFoods(foods: List<String>): Uni<Void> = Uni.createFrom().completionStage(
        CompletableFuture.supplyAsync {
            println("Eat   > Start: $foods")
            Thread.sleep(100)
            println("Eat   > Done: $foods")
            null
        }
    )
    fun eatFoodsBlock(foods: List<String>) { eatFoods(foods).await().indefinitely() }

    //-------

    fun getFruits(): Multi<String> = Multi.createFrom().emitter { em ->
        CompletableFuture.supplyAsync {
            println("Fruits > Start")
            Thread.sleep(50)
            em.emit("Apple")
            Thread.sleep(50)
            em.emit("Orange")
            println("Fruits > Done")
            em.complete()
        }
    }

    fun getBerries(): Multi<String> = Multi.createFrom().emitter { em ->
        CompletableFuture.supplyAsync {
            println("Berries > Start")
            Thread.sleep(25)
            em.emit("Strawberry")
            Thread.sleep(25)
            em.emit("Blueberry")
            println("Berries > Done")
            em.complete()
        }
    }

    fun makeJuice(fruit: String): Uni<String> = Uni.createFrom().completionStage(
        CompletableFuture.supplyAsync {
            println("Juice  > Start: $fruit")
            Thread.sleep(100)
            println("Juice  > Done: $fruit")
            "${fruit}_Juice"
        }
    )

    //-------

    fun getAlphabet(): Multi<List<String>> = Multi.createFrom().emitter { em ->
        CompletableFuture.supplyAsync {
            println("Alphabet > Start")
            Thread.sleep(25)
            em.emit(listOf("A","B","C"))
            Thread.sleep(25)
            em.emit(listOf("D","E"))
            Thread.sleep(25)
            em.emit(listOf("F"))
            println("Alphabet > Done")
            em.complete()
        }
    }

    fun lowerCase(list: List<String>): Multi<String> = Multi.createFrom().emitter { em ->
        CompletableFuture.supplyAsync {
            println("Lower > Start: $list")
            list.forEach {
                Thread.sleep(25)
                em.emit(it.toLowerCase())
            }
            println("Lower > Done")
            em.complete()
        }
    }

    fun getCart(): Uni<List<String>> = Uni.createFrom().context { ctx ->
        println("Cart > Get: ${ctx.get<String>("ID")}")
        Uni.createFrom().item(listOf("Item1", "Item2"))
    }

    fun getReceipt(items: List<String>): Uni<String> = Uni.createFrom().context { ctx ->
        Uni.createFrom().item("Customer: ${ctx.get<String>("ID")} | Items: $items")
    }

    //-------

    private val bucket = listOf(
        "ABC", "BCD", "CDE", "DEF", "FGH", "GHI", "HIJ", "IJK", "JKL", "LMN", "MNO", "NOP", "OPQ", "PQR", "QRS"
    )

    fun getItems(cursor: Int): Uni<Tuple2<List<String>, Int?>> {

        val completableFuture = CompletableFuture.supplyAsync {
            var end = cursor + 2
            var nc: Int? = end
            if (end >= bucket.size) {
                end = bucket.size
                nc = null
            }

            Thread.sleep(100)
            Tuple2.of(bucket.subList(cursor, end), nc)
        }

        return Uni.createFrom().completionStage(completableFuture)
    }
}