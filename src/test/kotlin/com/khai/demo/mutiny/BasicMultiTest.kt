package com.khai.demo.mutiny

import io.smallrye.mutiny.Multi
import org.junit.jupiter.api.Test

class BasicMultiTest {

    private val service = Service()

    /**
     * Reactive non-blocking operation using Multi
     */
    @Test
    fun basic() {
        println("===== BEGIN =====")

        service.getFruits()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(200)
    }

    /**
     * Converting Uni<List> to Multi
     */
    @Test
    fun fromUni() {
        println("===== BEGIN =====")

        service.getVeges()
            .onItem().transformToMulti { l -> Multi.createFrom().iterable(l) }
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(200)
    }

    /**
     * Converting Multi to Uni<List>
     */
    @Test
    fun toUni() {
        println("===== BEGIN =====")

        service.getFruits()
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(200)
    }

    /**
     * Chain Multi stream to a Uni
     */
    @Test
    fun chainWithUni() {
        println("===== BEGIN =====")

        service.getFruits()
            .onItem().transformToUniAndMerge { fruit -> service.makeJuice(fruit) }
//            .flatMap { fruit -> Multi.createFrom().uni(service.makeJuice(fruit)) }
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    /**
     * Concatenate two Multi streams (series)
     * First stream need to complete before second stream starts
     */
    @Test
    fun multiConcat() {
        println("===== BEGIN =====")

        Multi.createBy().concatenating()
            .streams(service.getFruits(), service.getBerries())
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    /**
     * Merge two Multi streams (parallel)
     * Both streams starts in together
     */
    @Test
    fun multiMerge() {
        println("===== BEGIN =====")

        Multi.createBy().merging()
            .streams(service.getFruits(), service.getBerries())
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    /**
     * Combine two Multi streams by joining
     * Downstream will be StreamA.item-X + StreamB.item-X
     */
    @Test
    fun multiCombine() {
        println("===== BEGIN =====")

        Multi.createBy().combining()
            .streams(service.getFruits(), service.getBerries())
            .using { f -> f.joinToString("&") }
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    @Test
    fun multiRepeat() {
//        Multi.createBy().repeating()
        //TODO: Complete Task:
        // https://gitlab.com/matrixx-services/common/asynchronous-programming-challange/-/blob/master/src/test/java/com/lwlee2608/async/AsynsTest.java#L134
    }
}