package com.khai.demo.mutiny

import io.smallrye.mutiny.Context
import org.junit.jupiter.api.Test

class AdvanceTest {

    private val service = Service()

    /**
     * Multi that returns List of alphabet
     */
    @Test
    fun demoStream() {
        println("===== BEGIN =====")

        service.getAlphabet()
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    /**
     * Chain Multi to a Uni and concatenate result (series)
     * Each item emitted from Multi will trigger Uni in series
     * First item needs to complete before second item starts
     * Order will be preserved
     */
    @Test
    fun concatMap() {
        println("===== BEGIN =====")

        service.getAlphabet()
            .concatMap { l -> service.lowerCase(l) } //transformToUniAndConcat
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    /**
     * Chain Multi to a Uni and merge result (parallel)
     * Each item emitted from Multi will trigger Uni immediately
     * Order will not be preserved
     */
    @Test
    fun flatMap() {
        println("===== BEGIN =====")

        service.getAlphabet()
            .flatMap { l -> service.lowerCase(l) } //transformToUniAndMerge
            .collect()
            .asList()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(500)
    }

    /**
     * Using of Context in mutiny pipeline
     * Accessing data in Context by operation within subscription
     */
    @Test
    fun context() {
        println("===== BEGIN =====")

        val ctx = Context.of("ID", "ABC123")

        service.getCart()
            .chain { items -> service.getReceipt(items) }
            .subscribe()
            .with(ctx,
                { res -> println("Result: $res") },
                { err -> println("Failed: $err") }
            )

        println("====== END ======")
        Thread.sleep(100)
    }
}