package com.khai.demo.mutiny

import io.smallrye.mutiny.Uni
import org.junit.jupiter.api.Test

class BasicUniTest {

    private val service = Service()

//    @Test
//    fun imperative() {
//        println("===== BEGIN =====")
//
//        val veges = service.getVegesBlock()
//        println("Result: $veges")
//
//        println("====== END ======")
//    }

    /**
     * Reactive non-blocking operation using Uni
     */
    @Test
    fun reactive() {
        println("===== BEGIN =====")

        service.getVeges()
            .subscribe()
            .with { println("Result: $it") }

        println("====== END ======")
        Thread.sleep(200)
    }

    /**
     * Block a reactive operation
     */
    @Test
    fun reactiveBlocking() {
        println("===== BEGIN =====")

        val veges = service.getVeges().await().indefinitely()
        println("Result: $veges")

        println("====== END ======")
    }

//    @Test
//    fun imperativeChain() {
//        println("===== BEGIN =====")
//
//        val veges = service.getVegesBlock()
//        service.eatFoodsBlock(veges)
//
//        println("====== END ======")
//    }

    /**
     * Chaining two Unis together in series
     */
    @Test
    fun reactiveChain() {
        println("===== BEGIN =====")

        service.getVeges()
            .chain { foods -> service.eatFoods(foods) }
            .subscribe()
            .with { println("Yummy!") }

        println("====== END ======")
        Thread.sleep(300)
    }

    /**
     * Chaining three Unis together in series
     */
    @Test
    fun moreChain() {
        println("===== BEGIN =====")

        service.getVeges()
            .chain { greens -> service.cookFoods(greens) }
            .chain { foods -> service.eatFoods(foods) }
            .subscribe()
            .with { println("Yummy!") }

        println("====== END ======")
        Thread.sleep(300)
    }

    /**
     * Combine two Unis and execute in parallel
     * Then chain both result to another Uni
     */
    @Test
    fun parallelChain() {
        println("===== BEGIN =====")

        Uni.combine().all()
            .unis(
                service.getVeges(),
                service.getMeats()
            ).asTuple()
            .map { tuple -> tuple.item1 + tuple.item2 }
            .chain { foods -> service.eatFoods(foods) }
            .subscribe()
            .with { println("Yummy!") }

        println("====== END ======")
        Thread.sleep(300)
    }

    /**
     * Combine two Unis and execute in parallel
     * Chain both Unis' response to another Uni
     * Then chain both result to yet another Uni
     */
    @Test
    fun parallelNestedChain() {
        println("===== BEGIN =====")

        Uni.combine().all()
            .unis(
                service.getVeges()
                    .chain { veges -> service.cookFoods(veges) },
                service.getMeats()
                    .chain { meats -> service.cookFoods(meats) }
            ).asTuple()
            .map { tuple -> tuple.item1 + tuple.item2 }
            .chain { foods -> service.eatFoods(foods) }
            .subscribe()
            .with { println("Yummy!") }

        println("====== END ======")
        Thread.sleep(300)
    }
}